var fs = require('fs');
var synaptic = require('synaptic');
var mnist = require('mnist');
var express = require('express');
var app = express();
var port = 3000;

var Neuron = synaptic.Neuron,
    Layer = synaptic.Layer,
    Network = synaptic.Network,
    Trainer = synaptic.Trainer,
    Architect = synaptic.Architect;

app.use(express.static(__dirname + '/build'));

app.get("/", function(req, res) {
  res.sendFile(__dirname + '/build/index.html')
})

app.post("/trainNetwork", function (req, res) {

  var _net = new Architect.Perceptron(784, 100, 10);
  var set = mnist.set(10000, 5000);

  var trainingSet = set.training;

  var trainer = new Trainer(_net);

  trainer.train(trainingSet,{
    rate: .1,
    iterations: 50,
    error: .1,
    shuffle: true,
    cost: Trainer.cost.CROSS_ENTROPY,
    schedule: {
    every: 1, // repeat this task every 10 iterations
      do: function(data) {
        var date = new Date();
        console.info(data);
        console.info(date);
      }
    }
  });

  var exported = _net.toJSON();
  fs.writeFile(__dirname + '/backend/net.js', JSON.stringify(exported), 'utf8');
})

app.listen(port, function(error) {
  if (error) {
    console.error(error);
  } else {
    console.info("==> 🌎  Listening on port %s. Open up http://localhost:%s/ in your browser.", port, port);
  }
})

function httpGet(theUrl)
{
	var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false );
    xmlHttp.send( null );
    return xmlHttp.responseText;
}
