//переменные

var gulp = require('gulp'), // Сообственно Gulp JS
  uglify = require('gulp-uglify'), // Минификация JS
  concat = require('gulp-concat'), // Склейка файлов
  concatCss = require('gulp-concat-css');
  //imagemin = require('gulp-imagemin'), // Минификация изображений (tiny png лучше)
  csso = require('gulp-csso'), // Минификация CSS
  sass = require('gulp-sass'), // Конвертация SASS (SCSS) в CSS
  clean = require('gulp-clean'), // очистка директорий
  autoprefixer = require('gulp-autoprefixer'), //автоматическая расстановка префиксов
  runSequence = require('run-sequence'), //синхронизация выполнения задач
  rigger = require('gulp-rigger'), //использование шаблонов
  csslint = require('gulp-csslint'),
  browserify = require('gulp-browserify'),
  es6ify = require('es6ify');

//пути
var path = {
  build: {
    html: 'build/',
    js: 'build/js/',
    css: 'build/css/',
    img: 'build/img/',
    fonts: 'build/fonts/',
    browserify: 'build/js/'
  },
  src: {
    html_template: 'src/views/index.html',
    lint_config:'lint_config.json'
  },
  watch: {
    html: 'src/views/**/*.html',
    js: 'src/scripts/**/*.js',
    babel: 'src/scripts/**/*.js',
    style: 'src/styles/**/*.scss',
    img: 'src/images/**/*.*',
    fonts: 'src/fonts/**/*.*',
    browserify: 'src/scripts/app.js'
  },
  clean: 'build'
};


// Задача clean
gulp.task('clean', function () {
  return gulp.src('build', {read: false})
      .pipe(clean());
});

// Задача html
gulp.task('html', function() {
  gulp.src(path.watch.html)
    .pipe(rigger())
    .pipe(gulp.dest(path.build.html));
});

// Задача sass
gulp.task('sass', function() {
  gulp.src(path.watch.style)
    .pipe(sass().on('error', sass.logError))
    .pipe(csso())
    .pipe(autoprefixer({
      browsers: ['last 3 versions'],
      cascade: false
    }))
    .pipe(concatCss("style.min.css", {rebaseUrls: false}))
    .pipe(gulp.dest(path.build.css))
    // .pipe(csslint())
    // .pipe(csslint.reporter());
});

// Задача js
gulp.task('js', function() {
    gulp.src(path.watch.browserify)
      .pipe(browserify({
        insertGlobals : true,
        debug : (process.env.NODE_ENV != 'production') ? 'production' : 'development',
        add: es6ify.runtime,
        transform: es6ify
      }))
      .pipe(gulp.dest(path.build.browserify))
});

// Задача images
gulp.task('images', function() {
  gulp.src(path.watch.img)
    //.pipe(imagemin())
    .pipe(gulp.dest(path.build.img))

});

// Задача font
gulp.task('font', function() {
  gulp.src(path.watch.fonts)
    .pipe(gulp.dest(path.build.fonts))
});

// Задача watch
gulp.task('watch', function() {
  gulp.watch(path.watch.html, ['html']);
  gulp.watch(path.watch.style, ['sass']);
  gulp.watch(path.watch.js, ['js']);
  gulp.watch(path.watch.img, ['images']);
});

//список задач 
var tasks = {
  development: ['html','font', 'sass', 'js', 'images', 'watch'],
  production: ['html','font', 'sass', 'js', 'images']
}

var env = (process.env.NODE_ENV == 'production') ? 'production' : 'development';

gulp.task('default', function(callback) {
  runSequence('clean', tasks[env], callback);
});
