class MyCanvas {

    constructor(containerClassName) {
    	this._container = document.getElementsByClassName(containerClassName);
    	this._canvas;
    	this._context;
    	this._isDrawing = false;
        console.log("Создан экземпляр MyCanvas! Мммм... сахарок)");
    }

    init(){
    	console.log(this._container[0]);
    	this._container[0].innerHTML = '<canvas id="myCanvas" width="280" height="280"></canvas>';
        this._container[0].innerHTML += '<canvas id="dataCanvas" width="280" height="280"></canvas>';
    	this._canvas = document.getElementById("myCanvas");
	    this._context = this._canvas.getContext('2d');
        this._canvasD = document.getElementById("dataCanvas");
        this._contextD = this._canvasD.getContext('2d');
		this.addEventListeners();
    }

    refresh(){
    	this._context.clearRect (0, 0, this._canvas.width, this._canvas.height);
    	this._context.beginPath();
        this._contextD.clearRect (0, 0, this._canvasD.width, this._canvasD.height);
        this._contextD.beginPath();
    }

    draw(e){
    	if(this._isDrawing){
    		let pos = getMousePos(this._canvas, e);
		    let posx = pos.x;
		    let posy = pos.y;

		    this._context.fillStyle = "#000000";
            this._context.beginPath();
            this._context.arc(posx, posy, 5, 0, 2 * Math.PI, false);
            this._context.fillStyle = 'black';
            this._context.fill();
    	}
	}

    dataDraw(data){
        let i = 0;
        let j = 0;
        this._contextD.fillStyle = "#000000";

        for (i = 0; i < 28; i++){
            for (j = 0; j < 28; j++){
                if(data[i][j] != 0){
                    this._contextD.fillRect (i*10, j*10, 10, 10);
                }
            }
        }
    }

    startDrawing(){
		this._isDrawing = true;
	}

    stopDrawing(){
		this._isDrawing = false;
	}

	addEventListeners(){
		this._canvas.addEventListener("mousemove", this.draw.bind(this), false);
		this._canvas.addEventListener("mousedown", this.startDrawing.bind(this), false);
		this._canvas.addEventListener("mouseup", this.stopDrawing.bind(this), false);
	}

    static loadAll() {
        console.log("Загружаем все MyCanvas...");
    }

    pictureToArr(){
    	let width = this._canvas.width;
    	let height = this._canvas.height;
        let outputArr = [];
    	let i = 0;
        let j = 0;
        let pixelValue = 0;
    	let colorValue = 0;
        let newIn = 0;
    	let inputArr = this._context.getImageData(0, 0, width, height);

        //prepare arrays
            var n = width, m = height;
            var compressedArr = [];
            for (i = 0; i < m; i++){
                compressedArr[i] = [];
                for (j = 0; j < n; j++){
                    compressedArr[i][j] = 0;
                }
            }

            var nCompressed2 = 28, mCompressed2 = height;
            var compressedArr2 = [];
            for (i = 0; i < mCompressed2; i++){
                compressedArr2[i] = [];
                for (j = 0; j < nCompressed2; j++){
                    compressedArr2[i][j] = 0;
                }
            }

            var nCompressed3 = 28, mCompressed3 = 28;
            var compressedArr3 = [];
            for (i = 0; i < mCompressed3; i++){
                compressedArr3[i] = [];
                for (j = 0; j < nCompressed3; j++){
                    compressedArr3[i][j] = 0;
                }
            }

        //end array preparing

        //proceed arrays

        	for (i = 0; i < inputArr.data.length; i++) {
        		colorValue = inputArr.data[i];
        		pixelValue += colorValue;
        		if(i % 4 == 3){
        			if(pixelValue != 0){
        				compressedArr[Math.floor(newIn % 280)][Math.floor(newIn/280)] = 1;
        			} else {
        				compressedArr[Math.floor(newIn % 280)][Math.floor(newIn/280)] = 0;
        			}
                    newIn++;
        			pixelValue = 0;
        		}
    		}  

            pixelValue = 0;

            for (i = 0; i < 280; i++) {
                pixelValue = 0;
                for (j = 0; j < 280; j++) {
                    pixelValue += compressedArr[i][j]; 
                    if(i % 10 == 9){
                        if(pixelValue != 0){
                            compressedArr2[Math.floor(i/10)][j] = 1;
                        } else {
                            compressedArr2[Math.floor(i/10)][j] = 0;
                        }
                       pixelValue = 0;
                    }
                }
            }

            pixelValue = 0;

            for (i = 0; i < 28; i++) {
                for (j = 0; j < 280; j++) {
                    pixelValue += compressedArr2[i][j]; 
                    if(j % 10 == 9){
                        if(pixelValue != 0){
                            compressedArr3[i][Math.floor(j/10)] = 1;
                        } else {
                            compressedArr3[i][Math.floor(j/10)] = 0;
                        }
                       pixelValue = 0;
                    }
                }
            }
            
        //end arrays preparing

        this.dataDraw(compressedArr3);

        // 2 dymentions array to 1 dymention
        for (j = 0; j < 28; j++) {
            for (i = 0; i < 28; i++) {
                outputArr[i + j*28] =  compressedArr3[i][j];
            }
        }

    	return(outputArr);
    }

}

function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
}

export {MyCanvas};