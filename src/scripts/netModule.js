var synaptic = require('synaptic');
var mnist = require('mnist');
//var netJson = require('./trainedNet784v300v10i20d70p20');
// var netJson = require('./trainedNet784v30v10i20d8000p5000');
//var netJson = require('./trainedNet784v50v10i50d8000p5000');
var netJson = require('./trainedNet784v100v10i50d10000p5000');
var traningSourse = 'fromFile';
var axios = require('axios');

var Neuron = synaptic.Neuron,
    Layer = synaptic.Layer,
    Network = synaptic.Network,
    Trainer = synaptic.Trainer,
    Architect = synaptic.Architect;

class MyNetwork {

    constructor() {
    	this._netTrained = false;
    	this._net;
 	}

 	init(traningSourse){
 		if(traningSourse == 'localStorage'){
			let net = JSON.parse(localStorage.getItem('network'));
			this._net = Network.fromJSON(net);
			if(this._net){this._netTrained = true;};
		} else if (traningSourse == 'fromFile') {
			let net = netJson;
			this._net = Network.fromJSON(net);
			if(this._net){this._netTrained = true;};
		} else {
			this._netTrained = false;
		}
 	}

 	train(){
 		let request = 'train';
 		axios.post('/trainNetwork', request)
		.then(result => {
			dispatch({
				type: NETWORK_TRAINED,
				payload: {
					currencies: result.data
				}
			})
		})
 	}

 	testNet(){
		var accuracy = 0;
		let i;
		let passI = 0;
		this._testSet = mnist.set(0, 50);
		for(i = 1; i < 50; i++){
			let testSet = this._testSet.test;
			let input = result_array_to_integer(this._net.activate(testSet[i].input));
			let output = result_array_to_integer(testSet[i].output);

			console.log('input: ' + input + ' ; output: ' + output);

			if(input == output){
				passI++
				
			} else {console.log('ERROR!!');}


			accuracy = passI/i;
		}

		console.log('Accoracy: ' + accuracy);
 	}

 	recognite(dataForRecognition){
 		if(this._netTrained){
			let recognitionResult = this._net.activate(dataForRecognition);
			let recognitionTestData = 0;
			return {recognitionResult, recognitionTestData};
 		} else {
 			console.log('There is no network to recognite');
 			return false;
 		}
 	}

 	saveNetToLS(){
 		let exported = this._net.toJSON();
		localStorage.setItem('network', JSON.stringify(exported));
		console.log('net saved to LS!');
 	}

 	demonstrateMnist(){
 		let set = mnist.set(1, 1);
 		console.log(set);
 	}

	proceedArray(array){
		var x = 0;
		var len = array.length

		while(x < len){ 
			array[x] = parseInt(array[x].toFixed()); 
			x++
		}

		return array;
	}

}

	function result_array_to_integer(array) {
		let x = 0;
		let len = array.length
		let number = 0;

		while(x < len){
			if(array[x] > array[number]){
				number = x;
			}
			x++
		}

		return number;
	} 

export {MyNetwork};