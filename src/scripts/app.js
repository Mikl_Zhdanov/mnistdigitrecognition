// let mnist = require('mnist');
import {MyNetwork} from "./netModule";
import {MyCanvas} from "./canvasModule";
import {MainView} from "./mainView";

let submitBtn = document.getElementsByClassName('submit-btn');
let clearBtn = document.getElementsByClassName('clear-btn');
let trainBtn = document.getElementsByClassName('train-btn');
let testBtn = document.getElementsByClassName('test-btn');

let canvas1 = new MyCanvas('canvas-container');
let network1 = new MyNetwork();
let mainView1 = new MainView();

network1.init('fromFile');
network1.recognite([]);
//network1.demonstrateMnist();
canvas1.init();
mainView1.init();

submitBtn[0].addEventListener('click', function(e){
	let dataForRecognition = canvas1.pictureToArr();
	mainView1.represent_result(network1.recognite(dataForRecognition));
});

clearBtn[0].addEventListener('click', function(e){
	canvas1.refresh();
});

trainBtn[0].addEventListener('click', function(e){
	network1.train();
});

testBtn[0].addEventListener('click', function(e){
	network1.testNet();
});