class MainView {

    constructor() {
		this.error = 1;
		this.trueAttempt = 0;
    }

    init(){
    	this._preloader = document.querySelector('.preloader');
    	this._netContainer = document.querySelector('.net-container');
    	this._container = document.querySelector('.result-container-graph');
		this._result = document.getElementById('result');
		this._menuToggleBtn = document.querySelector('.toggle-btn');
		this.addEventListeners();
		this.hidePreloader();
    }

    represent_result(data){
    	let result = data.recognitionResult;
		let elem;
		let att;
		this._result.innerHTML = result_array_to_integer(result);
		this._container.innerHTML = "";

		for (var value of result) {
		    elem = document.createElement("div");
		    att = document.createAttribute("style"); 
		    att.value = "height:" + (value*100) + "%";
			elem.setAttributeNode(att); 
		    this._container.appendChild(elem);

		}
	}

	hidePreloader(){
		this._preloader.classList.add('hidden');
	}

	addEventListeners(){
		this._menuToggleBtn.addEventListener('click', function(){
			console.log(this._netContainer);
			this._netContainer.classList.toggle('hidden-container');
		}.bind(this));
	}
}

function result_array_to_integer(array) {
	let x = 0;
	let len = array.length
	let number = 0;

	while(x < len){
		if(array[x] > array[number]){
			number = x;
		}
		x++
	}

	return number;
} 

export {MainView};